import React from 'react';
// export const Nav00DataSource = {
//   wrapper: { className: 'header0 home-page-wrapper' },
//   page: { className: 'home-page' },
//   logo: {
//     className: 'header0-logo',
//     children: 'https://os.alipayobjects.com/rmsportal/mlcYmsRilwraoAe.svg',
//   },
//   Menu: {
//     className: 'header0-menu',
//     children: [
//       {
//         name: 'item0',
//         className: 'header0-item',
//         children: {
//           href: '#',
//           children: [
//             {
//               children: (
//                 <span>
//                   <p>首页</p>
//                 </span>
//               ),
//               name: 'text',
//             },
//           ],
//         },
//         subItem: [
//           {
//             name: 'sub0',
//             className: 'item-sub',
//             children: {
//               className: 'item-sub-item',
//               children: [
//                 {
//                   name: 'image0',
//                   className: 'item-image',
//                   children:
//                     'https://gw.alipayobjects.com/zos/rmsportal/ruHbkzzMKShUpDYMEmHM.svg',
//                 },
//                 {
//                   name: 'title',
//                   className: 'item-title',
//                   children: 'Ant Design',
//                 },
//                 {
//                   name: 'content',
//                   className: 'item-content',
//                   children: '企业级 UI 设计体系',
//                 },
//               ],
//             },
//           },
//           {
//             name: 'sub1',
//             className: 'item-sub',
//             children: {
//               className: 'item-sub-item',
//               children: [
//                 {
//                   name: 'image0',
//                   className: 'item-image',
//                   children:
//                     'https://gw.alipayobjects.com/zos/rmsportal/ruHbkzzMKShUpDYMEmHM.svg',
//                 },
//                 {
//                   name: 'title',
//                   className: 'item-title',
//                   children: 'Ant Design',
//                 },
//                 {
//                   name: 'content',
//                   className: 'item-content',
//                   children: '企业级 UI 设计体系',
//                 },
//               ],
//             },
//           },
//         ],
//       },
//       {
//         name: 'item1',
//         className: 'header0-item',
//         children: {
//           href: '#',
//           children: [
//             {
//               children: (
//                 <span>
//                   <p>解决方案</p>
//                 </span>
//               ),
//               name: 'text',
//             },
//           ],
//         },
//       },
//       {
//         name: 'item2',
//         className: 'header0-item',
//         children: {
//           href: '#',
//           children: [
//             {
//               children: (
//                 <span>
//                   <span>
//                     <p>价格</p>
//                   </span>
//                 </span>
//               ),
//               name: 'text',
//             },
//           ],
//         },
//       },
//       {
//         name: 'item3',
//         className: 'header0-item',
//         children: {
//           href: '#',
//           children: [
//             {
//               children: (
//                 <span>
//                   <span>
//                     <span>
//                       <p>关于我们</p>
//                     </span>
//                   </span>
//                 </span>
//               ),
//               name: 'text',
//             },
//           ],
//         },
//       },
//       {
//         name: 'item~khpykfp65oe',
//         className: 'header0-item',
//         children: {
//           href: '#',
//           children: [
//             {
//               children: (
//                 <span>
//                   <p>登录系统</p>
//                 </span>
//               ),
//               name: 'text',
//             },
//           ],
//         },
//       },
//     ],
//   },
//   mobileMenu: { className: 'header0-mobile-menu' },
// };
export const Banner30DataSource = {
  wrapper: { className: 'banner3 khlgrr802ps-editor_css' },
  textWrapper: {
    className: 'banner3-text-wrapper',
    children: [
      {
        name: 'slogan',
        className: 'banner3-slogan khlgre0a0k9-editor_css',
        children: '一个既能打电话又能聊微信的系统 ',
        texty: true,
      },
      {
        name: 'name',
        className: 'banner3-name',
        children: (
          <span>
            <p>新一代呼叫中​心+智能微信在线客服 </p>
          </span>
        ),
      },
    ],
  },
};
export const Content10DataSource = {
  wrapper: { className: 'home-page-wrapper content1-wrapper' },
  OverPack: { className: 'home-page content1', playScale: 0.3 },
  imgWrapper: { className: 'content1-img', md: 10, xs: 24 },
  img: {
    children: 'https://zos.alipayobjects.com/rmsportal/nLzbeGQLPyBJoli.png',
  },
  textWrapper: { className: 'content1-text', md: 14, xs: 24 },
  title: {
    className: 'content1-title',
    children: (
      <span>
        <p>我们是谁？</p>
      </span>
    ),
  },
  content: {
    className: 'content1-content',
    children: (
      <span>
        <p>
          &nbsp; &nbsp; &nbsp; &nbsp;
          我们是一家人工智能技术与服务提供商，主营业务是基于微信生态，为客户提供集销售、市场和售后于一体的智能机器人客服系统，通过智能对话，客服系统，智能外呼和数据分析帮助企业快速获客，增加用户粘性并提高转化。将智能对话落地微信场景，
          国内极少的基于微信生态的智能对话服务商。
          客户覆盖教育、零售、保险、大健康等多个领域。
        </p>
      </span>
    ),
  },
};
export const Pricing10DataSource = {
  wrapper: { className: 'home-page-wrapper pricing1-wrapper' },
  page: { className: 'home-page pricing1' },
  OverPack: { playScale: 0.3, className: 'pricing1-content-wrapper' },
  titleWrapper: {
    className: 'pricing1-title-wrapper',
    children: [
      {
        name: 'title',
        children: (
          <span>
            <span>
              <p>服务与支持</p>
            </span>
          </span>
        ),
        className: 'pricing1-title-h1',
      },
    ],
  },
  block: {
    className: 'pricing1-block-wrapper',
    children: [
      {
        name: 'block0',
        className: 'pricing1-block',
        md: 8,
        xs: 24,
        children: {
          wrapper: { className: 'pricing1-block-box ' },
          topWrapper: { className: 'pricing1-top-wrapper' },
          name: {
            className: 'pricing1-name',
            children: (
              <span>
                <p>
                  <br />
                </p>
              </span>
            ),
          },
          money: {
            className: 'pricing1-money',
            children: (
              <span>
                <span>
                  <span>
                    <p>建议反馈</p>
                  </span>
                </span>
              </span>
            ),
          },
          content: {
            className: 'pricing1-content',
            children: (
              <span>
                <h3>
                  <b>
                    咨询热线：0451-57822233<br />咨询邮箱：576446831@qq.com
                  </b>
                </h3>
              </span>
            ),
          },
          line: { className: 'pricing1-line' },
          buttonWrapper: {
            className: 'pricing1-button-wrapper',
            children: {
              a: {
                className: 'pricing1-button',
                href: '#',
                children: (
                  <span>
                    <p>立即咨询</p>
                  </span>
                ),
              },
            },
          },
        },
      },
      {
        name: 'block1',
        className: 'pricing1-block',
        md: 8,
        xs: 24,
        children: {
          wrapper: { className: 'pricing1-block-box active' },
          topWrapper: { className: 'pricing1-top-wrapper' },
          name: {
            className: 'pricing1-name',
            children: (
              <span>
                <p>
                  <br />
                </p>
              </span>
            ),
          },
          money: {
            className: 'pricing1-money',
            children: (
              <span>
                <span>
                  <p>售前咨询</p>
                </span>
              </span>
            ),
          },
          content: {
            className: 'pricing1-content',
            children: (
              <span>
                <h3>
                  <b>
                    售前热线：0451-57822233<br />售前支持： 18800437586
                  </b>
                </h3>
              </span>
            ),
          },
          line: { className: 'pricing1-line' },
          buttonWrapper: {
            className: 'pricing1-button-wrapper',
            children: {
              a: {
                className: 'pricing1-button',
                href: '#',
                children: (
                  <span>
                    <p>立即咨询</p>
                  </span>
                ),
              },
            },
          },
        },
      },
      {
        name: 'block2',
        className: 'pricing1-block',
        md: 8,
        xs: 24,
        children: {
          wrapper: { className: 'pricing1-block-box ' },
          topWrapper: { className: 'pricing1-top-wrapper' },
          name: {
            className: 'pricing1-name',
            children: (
              <span>
                <span>
                  <span>
                    <p>
                      <br />
                    </p>
                  </span>
                </span>
              </span>
            ),
          },
          money: {
            className: 'pricing1-money',
            children: (
              <span>
                <p>售后支持</p>
              </span>
            ),
          },
          content: {
            className: 'pricing1-content',
            children: (
              <span>
                <h3>
                  <b>
                    售后热线：0451-57822233<br />售后支持： 15546064262
                  </b>
                </h3>
              </span>
            ),
          },
          line: { className: 'pricing1-line' },
          buttonWrapper: {
            className: 'pricing1-button-wrapper',
            children: {
              a: {
                className: 'pricing1-button',
                href: '#',
                children: (
                  <span>
                    <p>立即咨询</p>
                  </span>
                ),
              },
            },
          },
        },
      },
    ],
  },
};
export const Contact00DataSource = {
  wrapper: { className: 'home-page-wrapper content10-wrapper' },
  Content: {
    className: 'icon-wrapper',
    children: {
      icon: {
        className: 'icon',
        children:
          'https://gw.alipayobjects.com/zos/rmsportal/zIUVomgdcKEKcnnQdOzw.svg',
        name: '主要图标',
      },
      iconShadow: {
        className: 'icon-shadow',
        children:
          'https://gw.alipayobjects.com/zos/rmsportal/WIePwurYppfVvDNASZRN.svg',
        name: '图标影阴',
      },
      url: { children: 'https://gaode.com/place/B0FFH3KPBX', name: '跳转地址' },
      title: { children: '大会地址', name: '弹框标题' },
      content: {
        children: '蚂蚁 Z 空间  浙江省杭州市西湖区西溪路556号',
        name: '弹框内容',
      },
    },
  },
};
export const Content110DataSource = {
  OverPack: {
    className: 'home-page-wrapper content11-wrapper khpylgvruhm-editor_css',
    playScale: 0.3,
  },
  titleWrapper: {
    className: 'title-wrapper',
    children: [
      {
        name: 'image',
        children:
          'https://gw.alipayobjects.com/zos/rmsportal/PiqyziYmvbgAudYfhuBr.svg',
        className: 'title-image',
      },
      {
        name: 'title',
        children: (
          <span>
            <p>立即领取免费试用机会</p>
          </span>
        ),
        className: 'title-h1',
      },
      {
        name: 'content',
        children: (
          <span>
            <p>
              <br />
            </p>
          </span>
        ),
        className: 'title-content',
      },
      {
        name: 'content2',
        children: (
          <span>
            <span>
              <span>
                <h2>最高时长可达7日</h2>
              </span>
            </span>
          </span>
        ),
        className: 'title-content',
      },
    ],
  },
  button: {
    className: '',
    children: {
      a: {
        className: 'button',
        href: '#',
        children: (
          <span>
            <p>免费试用</p>
          </span>
        ),
      },
    },
  },
};
export const Footer10DataSource = {
  wrapper: { className: 'home-page-wrapper footer1-wrapper' },
  OverPack: { className: 'footer1', playScale: 0.2 },
  block: {
    className: 'home-page',
    gutter: 0,
    children: [
      {
        name: 'block0',
        xs: 24,
        md: 6,
        className: 'block',
        title: {
          className: 'logo',
          children:
            'https://zos.alipayobjects.com/rmsportal/qqaimmXZVSwAhpL.svg',
        },
        childWrapper: {
          className: 'slogan',
          children: [
            {
              name: 'content0',
              children: 'Animation specification and components of Ant Design.',
            },
          ],
        },
      },
      {
        name: 'block1',
        xs: 24,
        md: 6,
        className: 'block',
        title: { children: '产品' },
        childWrapper: {
          children: [
            { name: 'link0', href: '#', children: '产品更新记录' },
            { name: 'link1', href: '#', children: 'API文档' },
            { name: 'link2', href: '#', children: '快速入门' },
            { name: 'link3', href: '#', children: '参考指南' },
          ],
        },
      },
      {
        name: 'block2',
        xs: 24,
        md: 6,
        className: 'block',
        title: { children: '关于' },
        childWrapper: {
          children: [
            { href: '#', name: 'link0', children: 'FAQ' },
            { href: '#', name: 'link1', children: '联系我们' },
          ],
        },
      },
      {
        name: 'block3',
        xs: 24,
        md: 6,
        className: 'block',
        title: { children: '资源' },
        childWrapper: {
          children: [
            { href: '#', name: 'link0', children: 'Ant Design' },
            { href: '#', name: 'link1', children: 'Ant Motion' },
          ],
        },
      },
    ],
  },
  copyrightWrapper: { className: 'copyright-wrapper' },
  copyrightPage: { className: 'home-page' },
  copyright: {
    className: 'copyright',
    children: (
      <span>
        ©2018 by <a href="https://motion.ant.design">Ant Motion</a> All Rights
        Reserved
      </span>
    ),
  },
};
