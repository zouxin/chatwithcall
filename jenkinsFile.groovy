stage("git") {
   node {
     sh "git init"
     checkout scm
   }
}

stage("npm install") {
   node {
     sh('docker run --rm -v $(pwd):/data  -w /data schlez/docker-node-gyp ' + "sh -c 'npm --registry https://registry.npm.taobao.org install'")
   }
}

// stage("test") {
//    node {
//      sh('docker run --rm -v $(pwd):/data  -w /data node:10-alpine ' + "sh -c 'npm run test'")
//    }
// }

stage("build") {
   node {
     sh('docker run --rm -v $(pwd):/data  -w /data schlez/docker-node-gyp ' + "sh -c 'npm run-script build'")
   }
}

stage("publish") {
   node {
     sh('docker stop chatwithcall || true')
     sh('docker run --rm -p 3000:80 -d --name chatwithcall -v $(pwd)/dist:/usr/share/nginx/html/ -d nginx')
   }
}
